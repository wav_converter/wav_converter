<?php
    /// Ce fichier PHP contient l'ensemble des fonctions permettant de fournir les informations utiles au site convertisseur ///

    // Fonction permettant d'effectuer la connexion avec la BDD
	function connectMaBase(){
		$db_user = 'root'; // Utilisateur de la BDD 
		$user_pw = ''; // Mot de passe de la BDD
		try {
			$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
			// Informations comple©mentaires pour la connexion (host, le nom de la BDD)
			$bdd = new PDO('mysql:host=localhost;dbname=wav_converter;charset=utf8', $db_user, $user_pw, $pdo_options);
		}
		catch (Exception $e) {
			die('Erreur : ' . $e->getMessage());
		}
		return $bdd;					
	}

	// Fonction qui permet de récupérer l'ID de la ressource
	function recuperationID($link){
		$bdd=connectMaBase();
		$idRessource = 0;

		//Requete SQL
		$res = $bdd -> query("SELECT * FROM  ressources Where url = '".$link."'");

		while($data = $res->fetch())
		{   
			// Recuperation de l'id existante dans la BDD pour recherche de la ressource sur le serveur
			$idRessource = $data["ID"];
		}
		return $idRessource;
	}

	// Fonction permettant de vérifier si la ressource existe dans le dossier du serveur
	function verification($link){
		$bdd=connectMaBase();

		//Requete SQL
		$res = $bdd -> query("SELECT * FROM  ressources Where url = '".$link."'");
		$total = $res->rowCount();
		$donnees = $res->fetch();

		//Si la ressource existe
		if($total > 0){
			// Recuperation du fichier
			$rep = "../server/ressources";
			
			// Vérification si le dossier existe
				if($dossier = opendir($rep))
				{
					$idRessource = recuperationID($link);
					// Vérification que la lecture du dossier n'a pas retourné d'erreur
					while(false !== ($fichier = readdir($dossier)))
					{
						if($fichier == $idRessource.".wav")
						{
							if($idRessource != 0){
								header("Location:http://localhost/wav/server/ressources/".$idRessource.".wav");  
							}else{
								Echo "Error Serveur : Impossible de récupérer la ressource en base";
							}
						}
					}
					closedir($dossier);
				}else{
					echo "Error Serveur : Le dossier n'existe pas";
				}
		//Sinon conversion
		}else{
			// Appel serveur
			fopen ("http://localhost:8888/?url=".$link, "r");
			Sleep(15);
			
			$idRessource = recuperationID($link);

			if($idRessource != 0){
				header("Location:http://localhost/wav/server/ressources/".$idRessource.".wav");
			}else{
				Echo "Error Serveur : Impossible de récupérer la ressource en base";
			}
		}
	}

	function platform($i){
	    // Connection BDD
		$bdd=connectMaBase();
		
		//while($i < 5){
			//Requete SQL
			$res = $bdd -> query("SELECT Count(ID_platform) AS countDL, platform.libelle FROM ressources, platform WHERE ID_platform = ".$i." AND platform.ID = ".$i);
			$donnees = $res->fetch();
			
			// Stockage du resultat dans la variable $rs
			$platform = $donnees["libelle"];
			$nbDL = $donnees["countDL"];
					
			//Affichage du resultat
			echo $platform." : ".$nbDL;
		//}
		
	}
?>

