<?php
    include("./function.php");
?>

<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <title>WavConverter</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>

  <link rel="icon"  href="favi.ico" />
</head>
    <?php
        //Appel fonction quand clic dans le formulaire
        if (isset ($_POST['action'])){
            $link = $_POST['link'];
            $url = explode("&",$link);
            $urlConv = $url[0];
            $total = verification($urlConv);
        }
    ?>
<body>
    <!-- Image du site -->
    <div id="index-banner" class="parallax-container">
        <div class="parallax"><img src="img\164801159-studio-wallpapers.jpg" alt="Unsplashed background img 1"></div>
    </div>

    <div class="container">
        <div class="col s12 center">
            <div class="row">
                <div class="input-field inline">
                    <div class="card #fafafa grey lighten-4">
                         <!-- Cadre -->
                        <div class="card-content">
                            <span class="card-title" style="text-decoration: underline;"><B>Wav Converter</B></span>
                             <!-- Formulaire -->
                            <form name="converter" method="post">
                                <input id="link" type="text" name="link" class="validate">
                                <label for="link" data-error="wrong"></label>
                                <button class="btn waves-effect waves-light" type="submit" name="action">Convertir</button>
                            </form>

                            <br>
                            <span style="text-decoration: underline;"><B>Nombre de telechargement par plateforme</B></span><br><br>
                            <?php 
                                // Affichage des telechargements par plateforme
                                $i = 1;
                                while($i <= 5):
                                    platform($i);?><br><?php
                                    $i++;
                                endwhile;
                            ?>
                        </div>
                    </div>
                </div>

                <!-- Collapsible (Team, sujet, technologies) -->
                <ul class="collapsible" data-collapsible="accordion">
                    <li>
                        <div class="collapsible-header"><i class="material-icons">edit</i>Sujet</div>
                        <div class="collapsible-body">
                            <span>
                                L'idée est de réaliser un convertisseur sous forme de client web, capable via un webservice de lire (téléchargement ou streaming) n'importe quelle source audio en entré : Deezer, Spotify, Youtube, Vevo, Mp3 ... de détecter le format et de le convertir en fichier .wav.
                            </span>
                        </div>
                    </li>
                    <li>
                        <div class="collapsible-header"><i class="material-icons">supervisor_account</i>Team</div>
                        <div class="collapsible-body">
                            <span>
                                Notre team est composée de deux étudiants en Licence 3, d'un étudiant en Master 1 S2i et d'un dernier étudiant en Master 2 S2i. Notre objectif est de valoriser nos formations et de mettre en commun nos compétences.
                            </span>
                        </div>
                    </li>
                    <li>
                        <div class="collapsible-header"><i class="material-icons">settings</i>Technologies</div>
                        <div class="collapsible-body">
                            <span>
                                Pour le développement de notre interface, nous avons utilisé le framework CSS Materialize car il nous a paru intéressant de travailler avec une technologie jeune et moderne. Nous avons également complété le développement de l'IHM avec le langage de programmation PHP.<br><br>
                                Côté serveur, nous nous sommes orienté vers le langage de programmation JavaScript car c'est un langage dynamique et moderne mais aussi car il nous permet d'utiliser la palteform web NodeJS car il est capable de traiter une volumétrie importante de requêtes et son mode asynchrone permet de maximiser le traitement en le rendant plus rapide.<br><br>
                                Pour notre base de données, nous utilisons une base de données relationnelle. 
                            </span>
                        </div>
                    </li>
                </ul>
            </div>  
      </div>
    </div>

    <!-- footer -->
    <footer class="page-footer teal">
        <center><a href="https://plus.google.com/collections/featured"><img align="midle" width="30px" height="30" src="img\twitter.png">
                <a href="https://plus.google.com/collections/featured"><img align="midle" width="30px" height="30" src="img\google+.png">
                <a href="https://fr-fr.facebook.com/"><img align="midle" width="30px" height="30" src="img\facebook.png"></center></a>
		<center><font color="white">Copyright  WavConverter Company, 2018 © <br>All rights reserved</font></center><br>
    </footer>

    <!--  Scripts-->
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="js/materialize.js"></script>
    <script src="js/init.js"></script>

  </body>
</html>
