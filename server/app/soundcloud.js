exports.downloadVideoSoundcloud = function(data){
    const fs = require('fs');
    var mysql = require('mysql');
    var cmd = require('node-command-line');
    var IDressource = '';

    var url = data['url'];
    if (url != undefined || url != '') {
        // Connexion à la base de données
        var con = mysql.createConnection({
                host: "localhost",
                user: "root",
                password: "",
                database: "wav_converter"
            });

            con.connect(function(err) {
                if (err) throw err;
                console.log("Connected!");
                //on enregistre l'identifiant de la video dans la base de données
                var sql = "INSERT INTO `ressources`(`url`, `ID_platform`) VALUES ('"+data['url']+"',3)";
                con.query(sql, function (err, result) {
                    if (err) throw err;
                    // récupération de l'ID de la ressource ajoutée
                    IDressource = result.insertId;
                    console.log(IDressource);
                });
            });
        //on attend 500ms pour avoir le temps de récup l'id de la ressource
            setTimeout(function(){
                //on enregistre le fichier au format .wav sur le serveur dans le dossier /ressources
                cmd.run('youtube-dl '+data['url']+' -o ressources/'+IDressource+'.wav');
            }, 500);

        }
        else {
            return "Erreur url souncloud";
        }
    };
