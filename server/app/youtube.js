/**
 * Created by Julien on 15/01/2018.
 */

exports.downloadVideoYoutube = function(data){
    const fs = require('fs');
    const ytdl = require('ytdl-core');
    var mysql = require('mysql');

    var IDressource = '';

    var url = data['url'];
    if (url != undefined || url != '') {
        //on valide le lien youtube plus précisément
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
        var match = url.match(regExp);
        if (match && match[2].length == 11) {
            // Connexion à la base de données
            var con = mysql.createConnection({
                host: "localhost",
                user: "root",
                password: "",
                database: "wav_converter"
            });

            con.connect(function(err) {
                if (err) throw err;
                console.log("Connected!");
                //on enregistre l'identifiant de la video dans la base de données
                var sql = "INSERT INTO `ressources`(`url`, `ID_platform`) VALUES ('"+data['url']+"',1)";
                con.query(sql, function (err, result) {
                    if (err) throw err;
                    // récupération de l'ID de la ressource ajoutée
                    IDressource = result.insertId;
                    console.log(IDressource);
                });
            });
            //on attend 500ms pour avoir le temps de récup l'id de la ressource
            setTimeout(function(){
                //on enregistre le fichier au format .wav sur le serveur dans le dossier /ressources
                ytdl(data['url'], { filter: 'audioonly' })
                    .pipe(fs.createWriteStream('ressources/'+IDressource+'.wav'));
            }, 500);
        }
        else {
            return "Erreur url youtube";
        }
    }

};