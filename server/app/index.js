// app/index.js

var http = require('http');
var url = require('url');
var querystring = require('querystring');
var youtube = require('./youtube');
var vevo = require('./vevo');
var soundcloud = require('./soundcloud');
var vimeo = require('./vimeo');
var dailymotion = require('./dailymotion');

// Création du server
var server = http.createServer(function (requete, resultat) {
    var params = querystring.parse(url.parse(requete.url).query);
    var data;
    var statutCode = 200;

    /*on verifie que le lien soit un lien
     *1-youtube
     *2-soundcloud
     *3-vevo
     *4-vimeo
     *5-Dailymotion
     *Soundcloud n'utilise pas de syntaxe d'URL facile a valider
     *Vimeo et Vevo semblent utiliser un algorithme pour generer leurs URLs mais nous avons seulement pris le nom pour valider le lien dans un premier temps
     */

    if ('url' in params) {
        //les liens youtubes peuvent être de la forme "http://youtube.com/" ou "http://youtu.be/". On teste les deux.
        if (params['url'].indexOf('youtube') != -1 || params['url'].indexOf('youtu.be') != -1) {
            console.log('Youtube download !');
            data = youtube.downloadVideoYoutube(params);
        } else if (params['url'].indexOf('soundcloud') != -1) {
            console.log("Soundcloud download !");
            data=soundcloud.downloadVideoSoundcloud(params);
        } else if (params['url'].indexOf('vevo') != -1) {                
            console.log('Vevo download !');
            data=vevo.downloadVideoVevo(params);
        } else if (params['url'].indexOf('vimeo') != -1) {
            console.log('Vimeo download !');
            data=vimeo.downloadVideoVimeo(params);
        //les liens de videos dailymotion sont de la forme "dailymotion.com/video/". On ne prend que ces URLs (pas d'URL d'artiste par exemple)
        } else if (params['url'].indexOf('dailymotion.com/video/') != -1) {
            console.log('Dailymotion download !');
            data=dailymotion.downloadVideoDailymotion(params);
        } else {
            data = 'URL is not Youtube, Soundcloud, Vevo, Vimeo, Dailymotion';
        }
    }

    resultat.writeHead(statutCode,{"Content-Type": "text/plain; charset=UTF-8"});
    // renvoi du resultat
    resultat.end(data);
});

// serveur à l'écoute sur le port
server.listen(8888) ;